<!DOCTYPE html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="abigtech" />
    <meta name="robots" content="noodp" />
    <meta name="Description" content="AbigTECH is a best IT solutions provider based in Myanmar. Phone:+95 95174733,+95 9401526924,+95 9253245786, Email:info@abigtech.com,support@abigtech.com" />
    <meta name="Keyword" content="Best IT Soultion in Myanmar, Desktop Application Development, Web Application Development, Mobile Application Development, SEO, Pay Per Click, Email Marketing, Search Engine Marketing, Graphic Design, Digital Soultion in Myanmar" />

    <title>AbigTECH</title>
    
    <meta property="og:title" content="abigtech - best IT solution in Myanmar" />
    <meta property="og:description" content="AbigTECH is a best IT solutions provider based in Myanmar. Our business model focuses on creating own projects and on the other side, long-term strategic relations with our great clients. We will support high quality, cost effective and on time delivery as customer current and future business needs. " />
    <meta property="og:url" content="http://www.abigtech.com/" />
    <meta property="og:image" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="best IT solution in Myanmar" />

     <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Fonts & Icons
    ================================================== -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,300italic,100italic,300,200,100,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


    <!-- Material CSS -->
    <link rel="stylesheet" type="text/css"  href="assets/css/materialize.css">   

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="assets/css/style.css?v=1.0">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-81631305-1', 'auto');
        ga('send', 'pageview');
    </script>
    </head>
    <body>
        <?php 

            include_once 'header.php';
            include_once 'home.php';
            include_once 'footer.php';

        ?>

        <!-- javascript
        ================================================== -->
            <script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
            <script type="text/javascript" src="assets/js/materialize.min.js"></script>
            <script type="text/javascript" src="assets/js/template.js"> </script>
            
         <a href="#0" class="back-top">Top</a>   
	</body>

</html>
    
    
    