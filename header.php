<div class="navbar-fixed">
    <nav class="custom-nav">
            <div class="nav-wrapper">
                <a href="" class="brand-logo"><img src="assets/img/logo.png" class="responsive-img"/> </a>
                <a href="#" data-activates="mobileview" class="mobile-nav button-collapse"><i class="material-icons">menu</i></a>
                <ul class="big-nav right hide-on-med-and-down">
                    <li><a href="#home" class="page-scroll">Home</a></li>
                    <li><a href="#about" class="page-scroll">About us</a></li>
                    <li><a href="#services" class="page-scroll">services</a></li>
                    <li><a href="#portfolio" class="page-scroll">portfolio</a></li>
                    <li><a href="#contact" class="page-scroll">Contact us</a></li>
                </ul>
               <ul class="side-nav" id="mobileview">
                    <li class="mobile-logo"><a href=""><img class="mobile-nav-logo" src="assets/img/logo-mobile.png"/></a></li>
                    <li class="divider"></li>
                    <li><a href="#home" class="page-scroll">Home</a></li>
                    <li><a href="#about" class="page-scroll">About us</a></li>
                    <li><a href="#services" class="page-scroll">services</a></li>
                    <li><a href="#portfolio" class="page-scroll">portfolio</a></li>
                    <li><a href="#contact" class="page-scroll">Contact us</a></li>
                </ul>
            </div>
    </nav>
</div>