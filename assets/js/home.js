$(document).ready(function(){

    $('.parallax').parallax();
    $(".other-info").on("webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd", function(){
    	other_info();
	});
	$('.modal-trigger').leanModal();
 
});

$(window).resize(function(){
	$(".other-info").on("webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd", function(){
    	other_info();
    });
    $(".other-info p").on("webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd", function(){
    	other_info();
    });
});

function other_info(){
	$(".other-info").removeAttr("style");
	var w_width = parseInt($(window).innerWidth());
	if(w_width>600 && w_width<993){
		var other_num = $(".other-info").length;
		var max_height = 0;
		var count = 0;
		while(count<other_num){
			var new_height = parseInt($($(".other-info")[count]).innerHeight());
			if(new_height>max_height){
				max_height = new_height;
			}
			count++;
		}
		$(".other-info").css("height", max_height);
	}
}