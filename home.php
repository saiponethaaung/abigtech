<div id="main_section">
    
     <!-- Banner
    ================================================== -->
    <div  id="home" class="banner ">
        <div class="content"> 
            
            <img src="assets/img/logo.png"/> 
            <p>Best IT Solution for Next Generation</p>
        
        </div>
    </div>
    
    
     <!-- About Us
    ================================================== -->
    <section id="about" class="white_background ">
        <div class="container">
            <div class="row">
                <h4 class="section-header">About Us</h4>
                <div class="company-profile">
                    <div class="col l5 m5 s12">
                        <img src="assets/img/about.jpg" class="img-responsive" style="width: 100%;"/>
                    </div>
                    <div class="col l7 m7 s12">
                        <p>
                            <strong>Abig<span style="color:#C72026">TECH</span></strong> is a best IT solutions provider based in Myanmar. Our Company has expertise in all streams of <em>Desktop Application , Web Design, Web Application, Mobile Application, Search Engine Optimization(SEO),Search Engine Marketing(SEM) Digital Marketing.</em> 
                        </p>
                        <p>
                          Our business model focuses on creating own projects and on the other side, long-term strategic relations with our great clients. We will support high quality, cost effective and on time delivery as customer current and future business needs.
                        </p>
                    </div>
                </div>
            </div>
        </div>
         
    </section>
    
    
     <!-- Services
    ================================================== -->
    <section id="services" class="gray_background ">
        <div class="container">
            <div class="row">
                <h4 class="section-header">our services</h4>
                <div class="col l4 m6 s12 mini_services">
                    <i class="material-icons"> desktop_windows </i>
                    <h6>Desktop App</h6>
                    <p>We develop customized Windows applications using the latest development tools and technologies. We have experienced Windows and .NET developers at our team that are having great skills and talent to work over with Windows software development.</p>
                </div>
                <div class="col l4 m6 s12 mini_services">
                    <i class="material-icons">web</i>
                    <h6>Web App</h6>
                    <p>Having a website for your business is an advantage for modern economy. We have varity of choice for your desire. CMS, Framework, MVC OOP structure, web application and pure PHP. We are specializing on what you need.</p>
                </div>
                <div class="col l4 m6 s12 mini_services">
                    <i class="material-icons">smartphone</i>
                    <h6>Mobile App</h6>
                    <p>Android is the most used OS for mobile in these day and we provide a quality and performance of the application you need for your success.</p>
                </div>
                <div class="col l4 m6 s12 mini_services">
                    <i class="material-icons">color_lens</i>
                    <h6>Graphic Design</h6>
                    <p>Your business is unique. Business logo is one of the thing that attract the customer. We suggest you to use a unique, creative and attractive logo for your business.</p>
                </div>
                <div class="col l4 m6 s12 mini_services">
                    <i class="material-icons">group_add</i>
                    <h6>Search Engine Optimization</h6>
                    <p>AbigTECH is a best IT solutions provider based in Myanmar. Our Company has expertise in all streams of Window Application , Web Design and Web Application services, Mobile Application, Search Engine Optimization(SEO), Internet Marketing.</p>
                </div>
                <div class="col l4 m6 s12 mini_services">
                    <i class="material-icons">email</i>
                    <h6>Online Marketing</h6>
                    <p>AbigTECH is a best IT solutions provider based in Myanmar. Our Company has expertise in all streams of Window Application , Web Design and Web Application services, Mobile Application, Search Engine Optimization(SEO), Internet Marketing.</p>
                </div>
            </div>
        </div>
    </section>
    
     <!-- Portfolio
    ================================================== -->
    <div id="portfolio" class="white_background">
       <div class="overlay">
        <div class="container">
            <div class="row">
               <h4 class="section-header">Portfolio</h4>
               
               <div class="col l3 m4 s6 product">
                    <i class="large material-icons">web</i>
                    <h6>School Management</h6>
                    
                </div>
               <div class="col l3 m4 s6 product">
                    <i class="large material-icons">desktop_windows</i>
                    <h6>Prices Myanmar</h6>
                    
                </div>
               <div class="col l3 m4 s6 product">
                    <i class="large material-icons">smartphone</i>
                    <h6>Myanma Football</h6>
                    
                </div>
               <div class="col l3 m4 s6 product">
                    <i class="large material-icons">email</i>
                    <h6>Myanmar Exchange Rates</h6>
                    
                </div>
            </div>
        </div>
      </div>
    </div>
     
      <!-- Contact Us
    ================================================== -->
    <section id="contact" class="gray_background">
        <div class="container">
            <div class="row">
                <h4 class="section-header">Get in Touch</h4>
                
                <!-- Form -->
                <div class="col l6 m6 s12 contct_form">
                    <form class="">
                      <div class="input-field">
                        <input placeholder="Your Name" id="first_name" type="text" class="validate">
                        <label for="">Name</label>
                      </div>
                        
                      <div class="input-field">
                        <input placeholder="Your Email" id="first_name" type="text" class="validate">
                        <label for="">Email</label>
                      </div>
                        
                      <div class="input-field">
                        <input placeholder="Subject" id="first_name" type="text" class="validate">
                        <label for="">Subject</label>
                      </div>
                        
                      <div class="input-field">
                        <textarea rol="" col="" class="materialize-textarea"> </textarea>
                        <label for="" >Message</label>
                      </div>
                        
                      <div class="input-field">
                          <a class="waves-effect btn btnsend">send</a>
                      </div>
                        
                    </form>
                </div>
                
                <!-- Map -->
                <div class="col l6 m6 s12 contct_address">
                    
                    <table class="address">
                        <tr><td><i class="material-icons">home</i></td>
                            <td><span>University Avenue Condo A, Floor 10-07, New University Avenue, 11201 Bahan Township, Yangon, Myanmar</span></td>
                        </tr>
                        <tr><td><i class="material-icons">call</i></td>
                            <td>
                                <span>+95 95174733</span><br />
                                <span>+95 9401526924</span><br />
                                <span>+95 9253245786</span>
                            </td>
                        </tr>
                        <tr><td><i class="material-icons">mail</i></td>
                            <td><span><a href="mailto:info@abigtech.com">info@abigtech.com</a></span><br />
                                <span><a href="mailto:support@abigtech.com">support@abigtech.com</a></span>
                            </td>
                        </tr>
                    </table>
                    <!-- 
                    <table class="social">
                        <tr><td><i class="material-icons"> linkedin </i></td>
                            <td><i class="material-icons"> linkedin </i></td>
                            <td><i class="material-icons"> linkedin </i></td>
                        </tr>
                    </table>
                    -->
                    
                </div>
            </div>
        </div>
    </section>
      
</div>